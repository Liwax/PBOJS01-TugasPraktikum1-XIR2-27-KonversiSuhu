/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package konversisuhu;

/**
 *
 * @author ROG Liwax
 */
import java.util.Scanner;
class KonversiSuhu 
{
    double celcius;
    
    public static void main (String [] args) throws Exception
    {
         //deklarasi
        double cel;
        //pemanggilan class
        KonversiSuhu KS = new KonversiSuhu();
        Reamur R = new Reamur();
        Fahrenheit F = new Fahrenheit();
        Kelvin K = new Kelvin();
        //input
        Scanner scan = new Scanner(System.in);
        System.out.print("Celcius = ");
        cel = scan.nextDouble();
        //output
        System.out.println("=====================");
        System.out.println("KONVERSI SUHU CELCIUS");
        System.out.println("=====================");
        System.out.println("Celcius = "+ cel);
        System.out.println("Kelvin = "+ K.setKelvin(cel));
        System.out.println("Reamur = "+ R.setReamur(cel));
        System.out.println("Fahrenheit = "+ F.setFahrenheit(cel));
    }
}
